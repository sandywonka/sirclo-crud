# SIRCLO-CRUD

## Installation

```bash
pip install -r requirements.txt
```

```bash
./manage.py makemigrations
./manage.py migrate
./manage.py runserver
```

## Note
- App name "sirclo"
