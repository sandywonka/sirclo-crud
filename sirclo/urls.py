from . import views
from django.urls import path, include

urlpatterns = [
    path("create/", views.create, name="create"),
    path("", views.retrieve),
    path("index/", views.retrieve, name="retrieve"),
    path("detail/<id>/", views.detail, name="detail"),
    path("update/<id>/", views.update, name="update"),
    path("detail/<id>/delete/", views.delete, name="delete"),
]
