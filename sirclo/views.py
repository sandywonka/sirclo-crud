from django.shortcuts import render
from django.db.models import Avg, FloatField

from django.shortcuts import get_object_or_404, render, HttpResponseRedirect

from .models import Sirclo
from .forms import SircloForm


def create(request):
    context = {}
    form = SircloForm(request.POST or None)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect("/index/")
    context["form"] = form

    return render(request, "create.html", context)


def retrieve(request):
    context = {}
    context["dataset"] = Sirclo.objects.all().order_by("-tanggal")
    context["counter_maks"] = Sirclo.objects.all().aggregate(Avg("maks"))
    context["counter_mins"] = Sirclo.objects.all().aggregate(Avg("mins"))
    context["counter_avg"] = Sirclo.objects.all().aggregate(
        diff=Avg("maks", output_field=FloatField(), max_digits=3) - Avg("mins")
    )

    return render(request, "index.html", context)


def detail(request, id):
    context = {}
    context["data"] = Sirclo.objects.get(id=id)

    return render(request, "detail.html", context)


def update(request, id):
    context = {}
    obj = get_object_or_404(Sirclo, id=id)
    form = SircloForm(request.POST or None, instance=obj)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect("/detail/" + id)
    context["form"] = form

    return render(request, "update.html", context)


def delete(request, id):
    context = {}
    obj = get_object_or_404(Sirclo, id=id)
    if request.method == "POST":
        obj.delete()
        return HttpResponseRedirect("/")

    return render(request, "delete.html", context)
