from django.contrib.auth.models import AnonymousUser, User
from django.test import RequestFactory, TestCase

from sirclo.models import Sirclo


class TestModel(TestCase):
    def test_create(self):
        response = self.client.post(
            "/create/", {"tanggal": "2019-05-03", "maks": 43, "mins": 40}, follow=True
        )
        user = Sirclo.objects.first()

        self.assertEqual(user.maks, 43)
        self.assertEqual(user.mins, 40)
        self.assertEqual(response.status_code, 200)
