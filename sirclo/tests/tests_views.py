from django.contrib.auth.models import AnonymousUser, User
from django.test import RequestFactory, TestCase

from sirclo.views import *


class TestViews(TestCase):
    def setUp(self):
        # Every test needs access to the request factory.
        self.factory = RequestFactory()
        self.dummy = Sirclo.objects.create(id=1, tanggal="2019-05-03", maks=45, mins=43)

    def test_views_retrieve(self):
        # Create an instance of a GET request.
        request = self.factory.get("/index/")
        request.dummy = self.dummy
        response = retrieve(request)
        self.assertEqual(response.status_code, 200)

    def test_views_create(self):
        # Create an instance of a GET request.
        request = self.factory.post("/create/")
        request.dummy = self.dummy
        response = create(request)
        self.assertEqual(response.status_code, 200)

    def test_views_detail(self):
        # Create an instance of a GET request.
        request = self.factory.get("/detail/")
        request.dummy = self.dummy
        response = detail(request, id=1)
        self.assertEqual(response.status_code, 200)

    def test_views_update(self):
        # Create an instance of a GET request.
        request = self.factory.post("/update/")
        request.dummy = self.dummy
        response = update(request, id=1)
        self.assertEqual(response.status_code, 200)

    def test_views_delete(self):
        # Create an instance of a GET request.
        request = self.factory.post("/delete/", follow=True)
        request.dummy = self.dummy
        response = delete(request, id=1)
        self.assertEqual(response.status_code, 302)
