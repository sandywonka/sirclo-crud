from django.test import TestCase, Client
from django.urls import reverse, resolve
from sirclo.views import *


class TestUrls(TestCase):
    def setup(self):
        Sirclo.objects.create(id=1)

    def test_urls_create(self):
        url = reverse("create")
        self.assertEquals(resolve(url).func, create)

    def test_urls_retrieve(self):
        url = reverse("retrieve")
        self.assertEquals(resolve(url).func, retrieve)

    def test_urls_update(self):
        url = reverse("update", args=(1,))
        self.assertEquals(resolve(url).func, update)

    def test_urls_detail(self):
        url = reverse("detail", args=(1,))
        self.assertEquals(resolve(url).func, detail)

    def test_client_create(self):
        c = Client()
        response = c.post("/create/", {"tanggal": "12/03/2021", "maks": 55, "mins": 52})
        response.status_code

    def test_client_retrieve(self):
        c = Client()
        response = c.get("/index/")
        response.status_code

    def test_client_detail(self):
        c = Client()
        response = c.get(
            "/detail/", {"id": 15, "tanggal": "12/03/2021", "maks": 55, "mins": 52}
        )
        response.status_code

    def test_client_update(self):
        c = Client()
        response = c.get(
            "/update/", {"id": 15, "tanggal": "12/03/2021", "maks": 55, "mins": 52}
        )
        response.status_code

    def test_client_update(self):
        c = Client()
        response = c.get("/delete/", {"id": 15})
        response.status_code
