from django import forms
from .models import Sirclo


class SircloForm(forms.ModelForm):
    class Meta:
        model = Sirclo
        fields = [
            "tanggal",
            "maks",
            "mins",
        ]
